# Lomiri URL Dispatcher

## Introduction

This is a small handler to take URLs and do what is appropriate with them.
That could be anything from launching a web browser to just starting an
application.  This is done over DBus because application confinement doesn't
allow for doing it from a confined application otherwise.  It's important
the that applications can't know about each other, so this is a fire and forget
type operation.

## i18n: Translating Lomiri URL Dispatcher into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/lomiri-url-dispatcher

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
