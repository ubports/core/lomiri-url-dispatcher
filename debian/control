Source: lomiri-url-dispatcher
Section: gnome
Priority: optional
Maintainer: UBports developers <developers@ubports.com>
Build-Depends: cmake,
               cmake-extras (>= 1.2),
               dbus-test-runner,
               debhelper-compat (= 12),
               dh-apparmor,
               dh-python,
               gcovr,
               googletest,
               intltool,
               lcov,
               libapparmor-dev,
               libdbus-1-dev,
               libdbustest1-dev (>= 14.04.0),
               libglib2.0-dev,
               libgtest-dev,
               libjson-glib-dev,
               libsqlite3-dev,
               liblomiri-app-launch-dev (>= 0.1.3),
               libmir1client-dev | hello,
               python3,
               python3-dbusmock,
               python3-fixtures,
               python3-nose,
               python3-testtools,
               sqlite3,
               systemd,
Standards-Version: 4.5.1
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/core/lomiri-url-dispatcher

Package: lomiri-url-dispatcher
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
         lomiri-url-dispatcher-common (>= ${source:Version}),
# For the bad URL dialog
         qtchooser,
         qml-module-lomiri-components,
Description: Lomiri Operating Environment service for requesting URLs to be opened
 Allows applications to request a URL to be opened and handled by another
 process without seeing the list of other applications on the system or
 starting them inside its own Application Confinement.
 .
 This package provides a service for the Lomiri URL Dispatcher

Package: lomiri-url-dispatcher-common
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
Description: Lomiri Operating Environment service for requesting URLs to be opened (common files)
 Allows applications to request a URL to be opened and handled by another
 process without seeing the list of other applications on the system or
 starting them inside its own Application Confinement.
 .
 This package provides arch-indep files for Lomiri URL Dispatcher server
 and client (esp. locale files).

Package: lomiri-url-dispatcher-tools
Architecture: any
Depends: lomiri-url-dispatcher (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Tools for working with the Lomiri URL Dispatcher
 Allows applications to request a URL to be opened and handled by another
 process without seeing the list of other applications on the system or
 starting them inside its own Application Confinement.
 .
 This package provides tools for working with the Lomiri URL Dispatcher.

Package: lomiri-url-dispatcher-tools-gui
Architecture: any
Depends: lomiri-url-dispatcher (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         qtchooser,
         qml-module-lomiri-components-labs,
Description: GUI tools for working with the Lomiri URL Dispatcher
 Allows applications to request a URL to be opened and handled by another
 process without seeing the list of other applications on the system or
 starting them inside its own Application Confinement.
 .
 This package provides GUI tools for working with the Lomiri URL Dispatcher.

Package: liblomiri-url-dispatcher0
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
         lomiri-url-dispatcher-common (>= ${source:Version}),
Suggests: lomiri-url-dispatcher (= ${binary:Version}),
Description: Library for sending requests to the Lomiri URL Dispatcher
 Allows applications to request a URL to be opened and handled by another
 process without seeing the list of other applications on the system or
 starting them inside its own Application Confinement.
 .
 This package contains shared libraries to be used by applications.

Package: liblomiri-url-dispatcher-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: libglib2.0-dev,
         liblomiri-url-dispatcher0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Development files for consumers of the Lomiri URL Dispatcher
 Allows applications to request a URL to be opened and handled by another
 process without seeing the list of other applications on the system or
 starting them inside its own Application Confinement.
 .
 This package contains files that are needed to build applications.

Package: lomiri-url-dispatcher-testability
Architecture: all
Depends: ${misc:Depends},
         python3,
         python3-dbusmock,
         python3-dbus,
Description: Fake Lomiri URL Dispatcher for use in testing
 Allows applications to request a URL to be opened and handled by another
 process without seeing the list of other applications on the system or
 starting them inside its own Application Confinement.
 .
 This package contains a fake Lomiri URL Dispatcher for use in testing.
